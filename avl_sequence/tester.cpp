#include <cassert>
#include <cstdlib>
#include <ctime>
#include <iostream>
#include <vector>
#include <string>
#include <chrono>

#include "avl_sequence.cpp"

void uncheked_random_test(size_t N, size_t PREOCCUPIED) {
  using namespace std::chrono;
  auto start = high_resolution_clock::now();
  srand(time(0));
  AVLSequence<int> seq;

  for(size_t i = 0; i < PREOCCUPIED; ++i){
    int x = rand();
    size_t idx = seq.size() ? rand() % seq.size() : 0;
    seq.insert(idx, x);
  }
  auto occupy = high_resolution_clock::now();

  std::cout << "Inserteed #" << PREOCCUPIED << " elements in " << duration_cast<milliseconds>(occupy - start)  << std::endl;

  char c;
  size_t x;
  for(size_t i = 0; i < N; ++i){
    c = rand() % 3;
    x = seq.size() ? rand() % seq.size() : 0; 
    
    switch(c){
      case 0:{
        int val = rand();
        assert(seq.insert(x, val));
        break;
      }
      case 1:
        if(seq.size()){
          assert(seq.erase(x));
        }
        break;
      case 2:
        if(seq.size()) seq[x];
        break;
    }
  }
  auto stop = high_resolution_clock::now();
  std::cout << "Performed #" << N << " operations in " << duration_cast<milliseconds>(stop - occupy) << std::endl;
}

void random_test(size_t N, size_t PREOCCUPIED = 0){
  srand(time(0));
  AVLSequence<int> seq;
  std::vector<int> vec;
  vec.reserve(PREOCCUPIED);

  for(size_t i = 0; i < PREOCCUPIED; ++i){
    int x = rand();
    vec.push_back(x);
    seq.push_back(x);
    assert(vec[i] == seq[i]);
    assert(vec.size() == seq.size());
  }

  char c;
  size_t x;
  for(size_t i = 0; i < N; ++i){
    c = rand() & 1; // % 2
    x = seq.size() ? rand() % seq.size() : 0; 
    
    switch(c){
      case 0:{
        int val = rand();
        assert(seq.insert(x, val));
        vec.insert(vec.begin() + x, val);
        break;
      }
      case 1:
        if(seq.size()){
          assert(seq.erase(x));
          vec.erase(vec.begin() + x);
        }
        break;
    }
    assert(seq.size() == vec.size());
    assert(x >= seq.size() || seq[x] == vec[x]);
    size_t idx = seq.size() ? rand() % seq.size() : 1;
    assert(idx || seq[idx] == vec[idx]);
  }

  std::cout << "Passed random tests with " << N << " operations with " << PREOCCUPIED << " positions with vector verification" << std::endl;
}

void test3() {
  AVLSequence<std::string> seq;
  
  seq.push_front("Pirati");
  seq.push_front("Ahoj");
  assert(seq[0] == "Ahoj" && seq[1] == "Pirati");
  std::cout << "Passed 3rd local test" << std::endl;
}

void test2(){
  AVLSequence<int> seq;

  assert(seq.insert(0, 0));
  assert(seq.insert(1, 1));
  assert(seq.insert(2, 2));
  assert(seq[0] == 0);
  assert(seq[1] == 1);
  assert(seq[2] == 2);
  assert(seq.erase(2));
  assert(seq.erase(1));
  assert(seq.erase(0));

  assert(seq.insert(0, 0));
  assert(seq.insert(1, 1));
  assert(seq.insert(1, 2));
  assert(seq[0] == 0);
  assert(seq[1] == 2);
  assert(seq[2] == 1);
  assert(seq.erase(2));
  assert(seq.erase(1));
  assert(seq.erase(0));

  assert(seq.insert(0, 0));
  assert(seq.insert(0, 1));
  assert(seq.insert(0, 2));
  assert(seq[0] == 2);
  assert(seq[1] == 1);
  assert(seq[2] == 0);
  assert(seq.erase(2));
  assert(seq.erase(1));
  assert(seq.erase(0));

  assert(seq.insert(0, 0));
  assert(seq.insert(0, 1));
  assert(seq.insert(1, 2));
  assert(seq[0] == 1);
  assert(seq[1] == 2);
  assert(seq[2] == 0);
  assert(seq.erase(2));
  assert(seq.erase(1));
  assert(seq.erase(0));
  std::cout << "Passed 2nd local test" << std::endl;
}

void test1(){
  AVLSequence<int> seq;

  assert(seq.size() == 0);

  seq.push_back(12);
  assert(seq[0] == 12);

  seq.push_front(-1);
  assert(seq[0] == -1);

  assert(seq.insert(1, 100));
  assert(seq[1] == 100);
  assert(seq.size() == 3);

  assert(seq.pop_back());
  assert(seq[1] == 100);
  assert(seq.back() == seq[1]);

  assert(seq.pop_front());
  assert(seq[0] == 100);
  assert(seq.front() == seq[0]);

  assert(seq.erase(0));
  std::cout << "Passed 1st local test" << std::endl;
}

int main(){
  
  AVLSequence<int> seq;

  test1();
  test2();
  test3();

  random_test(10'000);
  random_test(10'000, 10'000);
  random_test(50'000, 50'000);
  random_test(50'000, 100'000);

  uncheked_random_test(100'000, 100'000);
  uncheked_random_test(1e6, 1e6);
  uncheked_random_test(10'000'000, 10'000'000);
}
