#ifndef AVL_SEQ_1953914143809284083
#define AVL_SEQ_1953914143809284083

#include <cstdlib>
#include <utility>

template<typename T>
class AVLSequence{
public:

  AVLSequence() = default;

  ~AVLSequence() noexcept{
    // TODO
  }

  T & operator[](size_t idx){
    // TODO
  }

  const T & operator[](size_t idx) const{
    // TODO
  }

  size_t size() const{
    // TODO
  }

  bool insert(size_t idx, T val){
    // TODO
  }

  bool erase(size_t idx){
    // TODO
  }



  void push_front(T val){
    // TODO
  }

  void push_back(T val){
    // TODO
  }

  bool pop_front(){
    // TODO
  }

  bool pop_back(){
    // TODO
  }

  T & back(){
    return (*this)[size() - 1];
  }

  T & front(){
    return (*this)[0];
  }

  const T & back() const{
    return (*this)[size() - 1];
  }

  const T & front() const{
    return (*this)[0];
  }

private:
  struct Node{
    Node(T val, Node * parent): m_val(std::move(val)), m_P(parent) {}
    Node * m_L = nullptr, *m_R=nullptr, *m_P = nullptr;
    size_t m_height = 1, m_weight = 1;
    T m_val{};
  };

  Node * m_Root = nullptr;
};


#endif //AVL_SEQ_1953914143809284083
