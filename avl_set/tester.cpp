#include <cassert>
#include <climits>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <set>
#include <string>
#include "avl.cpp"

void random_test(size_t N, int MIN, int MAX){
  srand(time(0));
  assert(MAX >= MIN);

  AVLSet<int> avl;
  std::set<int> stl;

  int x, RANGE = MAX - MIN;
  char c;
  for(size_t i = 0; i < N; ++i){
    c = rand() % 3;
    x = rand() % RANGE + MIN;

    switch(c){
      case 0:
        assert(avl.contains(x) == stl.contains(x));
        break;
      case 1:
        assert(avl.insert(x) == stl.insert(x).second);
        break;
      case 2:
        assert(avl.erase(x) == stl.erase(x));
        break;
    }
  }

  std::cout << "Passed random test on " << N << " operations in range ( " << MIN << " , " << MAX << " )" << std::endl;
}

void test3(){
  AVLSet<std::string> avl_str;

  assert(avl_str.insert("aaa"));
  assert(!avl_str.insert("aaa"));
  assert(avl_str.insert("bbb"));
  assert(avl_str.insert("ccc"));

  assert(avl_str.contains("aaa"));
  assert(avl_str.contains("bbb"));
  assert(avl_str.contains("ccc"));

  assert(avl_str.erase("aaa"));
  assert(avl_str.erase("bbb"));
  assert(avl_str.erase("ccc"));

  auto cmp = [](const std::string &l, const std::string &r) {
    return strcasecmp(l.c_str(), r.c_str()) < 0;
  };

  AVLSet<std::string, decltype(cmp)> avl_case;

  assert(avl_case.insert("aaa"));
  assert(!avl_case.insert("AAA"));
  assert(!avl_case.insert("AaA"));


  struct A{
    A(int x) : a(x) {}
    int a;
  };

  auto a_cmp = [](const A & l, const A & r){ return !(r.a % l.a);};

  AVLSet<A, decltype(a_cmp)> avl_temp;

  assert(avl_temp.insert(2));
  assert(avl_temp.insert(10));
  assert(!avl_temp.insert(3));

  std::cout << "Passed template tests" << std::endl;
}

void test2(){
  AVLSet<int> avl;

  assert(avl.insert(1));
  assert(avl.insert(10));
  assert(avl.insert(100));
  assert(avl.contains(1));
  assert(avl.contains(10));
  assert(avl.contains(100));
  assert(avl.erase(1));
  assert(avl.erase(10));
  assert(avl.erase(100));

  assert(avl.insert(100));
  assert(avl.insert(10));
  assert(avl.insert(1));
  assert(avl.contains(1));
  assert(avl.contains(10));
  assert(avl.contains(100));
  assert(avl.erase(1));
  assert(avl.erase(10));
  assert(avl.erase(100));

  assert(avl.insert(100));
  assert(avl.insert(1));
  assert(avl.insert(10));
  assert(avl.contains(1));
  assert(avl.contains(10));
  assert(avl.contains(100));
  assert(avl.erase(1));
  assert(avl.erase(10));
  assert(avl.erase(100));

  assert(avl.insert(1));
  assert(avl.insert(100));
  assert(avl.insert(10));
  assert(avl.contains(1));
  assert(avl.contains(10));
  assert(avl.contains(100));
  assert(avl.erase(1));
  assert(avl.erase(10));
  assert(avl.erase(100));

  std::cout << "Passed 2nd local test" << std::endl;
}

void test1(){
  AVLSet<int> avl;

  assert(!avl.contains(1));
  assert(avl.insert(1));
  assert(!avl.insert(1));
  assert(avl.contains(1));

  assert(!avl.contains(10));
  assert(avl.insert(10));
  assert(!avl.insert(10));
  assert(avl.contains(10));

  assert(!avl.contains(100));
  assert(avl.insert(100));
  assert(!avl.insert(100));
  assert(avl.contains(100));

  assert(!avl.contains(50));
  assert(avl.insert(50));
  assert(!avl.insert(50));
  assert(avl.contains(50));

  assert(!avl.contains(-10));
  assert(avl.insert(-10));
  assert(!avl.insert(-10));
  assert(avl.contains(-10));

  assert(!avl.contains(-2));
  assert(avl.insert(-2));
  assert(!avl.insert(-2));
  assert(avl.contains(-2));

  assert(avl.erase(10));
  assert(avl.erase(1));
  assert(avl.erase(-10));
  assert(avl.erase(100));
  assert(avl.erase(50));
  assert(avl.erase(-2));

  std::cout << "Passed 1st local test" << std::endl;
}

int main(){
  test1();
  test2();
  test3();

  random_test(100'000, -(1 << 20), (1 << 20));
  random_test(100'000, -100, 100);

  random_test(10'000'000, -(1 << 30), (1 << 30));
  random_test(10'000'000, -1000, 1000);

  random_test(10'000'000, INT_MIN, INT_MAX);
}
