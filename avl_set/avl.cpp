#ifndef AVL_SET_345897645345768908457689
#define AVL_SET_345897645345768908457689
#include <functional>

template<typename T, typename CMP = std::less<T>>
class AVLSet {
public:

  AVLSet(CMP cmp = CMP{}):
    m_cmp(cmp) {}

  bool insert(T val ){
    // TODO
  }

  bool erase(const T & val){
    // TODO
  }

  bool contains(const T & val) const {
    // TODO
  }

private:
  struct Node {
    T m_val {};
    Node *m_L = nullptr, *m_R = nullptr, *m_P = nullptr;
    size_t m_height = 0;
  };

  Node * m_Root = nullptr;
  CMP m_cmp {};

};
#endif //AVL_SET_345897645345768908457689
