#ifndef GRAPH_GEN_HJSHDKHASJKLHDIUFNSKAHUINAJKSHDJASKD
#define GRAPH_GEN_HJSHDKHASJKLHDIUFNSKAHUINAJKSHDJASKD

#include "graph.cpp"
#include "weighted_graph.cpp"
#include <cstdlib>
#include <ctime>

int randint(int MIN, int MAX){
  return (rand() % (MAX - MIN)) + MIN;
}

void generate_graph(DirGraph & graph, size_t NUM = 1000, int MIN = 0, int MAX = 51){
  srand(time(nullptr));
  for(size_t i = 0; i < NUM; ++i)
    graph.add_edge(randint(MIN, MAX), randint(MIN, MAX));
}


void generate_graph(DirWGraph & graph, size_t NUM = 1000, int MIN = 0, int MAX = 51, int MINWEIGHT = 0, int MAXWEIGHT = 200){
  srand(time(nullptr));
  srand(time(nullptr));
  for(size_t i = 0; i < NUM; ++i)
    graph.add_edge(randint(MIN, MAX), randint(MIN, MAX), randint(MINWEIGHT, MAXWEIGHT));
}

#endif//GRAPH_GEN_HJSHDKHASJKLHDIUFNSKAHUINAJKSHDJASKD
