#ifndef GRAPH_H_jasklfjaskjfldjasfjaldjfljlsdjfalsjdlfjadfjasd
#define GRAPH_H_jasklfjaskjfldjasfjaldjfljlsdjfalsjdlfjadfjasd
#include <map>
#include <set>
#include <deque>

class DirGraph{
public:
  DirGraph() = default;
  virtual ~DirGraph() = default;
  virtual bool add_edge(int from, int to) {
    return adj[from].insert(to).second;
  }

protected:
  std::map<int, std::set<int>> adj;
};

class UndirGraph : public virtual DirGraph{
public:
  UndirGraph() = default;
  ~UndirGraph() = default;

  bool add_edge(int from, int to){
    return DirGraph::add_edge(from, to) && DirGraph::add_edge(to, from);
  }
};

class DAG : public virtual DirGraph{
  DAG() = default;
  ~DAG() = default;

  bool add_edge(int from, int to){
    std::set<int> visited{to};
    std::deque<int> que{to};

    while(!que.size()){
      int curr = que.front();
      que.pop_front();

      for(int x : adj[curr]){
        if(x == from)
          return false;

        if(!visited.count(x)){
          visited.insert(x);
          que.push_back(x);
        }
      }
    }

    return DirGraph::add_edge(to, from);
  }

};

#endif //GRAPH_H_jasklfjaskjfldjasfjaldjfljlsdjfalsjdlfjadfjasd
