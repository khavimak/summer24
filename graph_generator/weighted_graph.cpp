
#ifndef WEIGHT_GRAPH_H_jasklfjaskjfldjasfjaldjfljlsdjfalsjdlfjadfjasd
#define WEIGHT_GRAPH_H_jasklfjaskjfldjasfjaldjfljlsdjfalsjdlfjadfjasd
#include <map>
#include <queue>
#include <set>

class DirWGraph{
public:
  DirWGraph() = default;
  virtual ~DirWGraph() = default;
  virtual bool add_edge(int from, int to, int weight) {
    return adj[from].emplace(to, weight).second;
  }

protected:
  struct Edge{
    int dst;
    int w;

    Edge(int adst, int aw):
      dst(adst), w(aw) {}

    friend bool operator<(const Edge & l, const Edge & r){
      return l.dst != r.dst ? l.dst < r.dst : l.w < r.w;
    }
  };
  std::map<int, std::set<Edge>> adj;
};

class UndirWGraph : public virtual DirWGraph{
public:
  UndirWGraph() = default;
  ~UndirWGraph() = default;

  bool add_edge(int from, int to, int weight){
    return DirWGraph::add_edge(from, to, weight) && DirWGraph::add_edge(to, from, weight);
  }
};

class WDAG : public virtual DirWGraph{
  WDAG() = default;
  ~WDAG() = default;

  bool add_edge(int from, int to, int weight){
    std::set<int> visited{to};
    std::priority_queue<Edge, std::vector<Edge>, PriorityCmp> que{};
    que.emplace(to, 0);

    while(!que.size()){
      Edge curr = que.top();
      que.pop();

      for(const Edge & e : adj[curr.dst]){
        if(e.dst == from)
          return false;

        if(!visited.count(e.dst)){
          visited.insert(e.dst);
          que.emplace(e.dst, curr.w + e.dst);
        }
      }
    }

    return DirWGraph::add_edge(to, from, weight);
  }

protected:

  struct PriorityCmp{
    bool operator()(const Edge & l, const Edge & r){
      return l.w > r.w;
    }
  };

};

#endif //WEIGHT_GRAPH_H_jasklfjaskjfldjasfjaldjfljlsdjfalsjdlfjadfjasd
