#ifndef BINARYHEAP_123567897323456789875434567432
#define BINARYHEAP_123567897323456789875434567432
#include <vector>
#include <functional>
#include <stdexcept>

template<typename T, typename CMP = std::less<T>, typename Container = std::vector<T>>
class BinaryHeap{
public:

  BinaryHeap(CMP cmp = CMP{}) :
    m_data(), m_cmp(cmp) {}

  ~BinaryHeap() = default;

  void push(T val){
    // TODO
  }

  const T & top() const{
    // TODO
  }

  T pop(){
    // TODO
  }

  size_t size() const {
    // TODO
  }

private:

  Container m_data{};
  CMP m_cmp;

};
#endif// BINARYHEAP_123567897323456789875434567432
