#include "binaryheap.cpp"
#include <cassert>
#include <iostream>
#include <queue>
#include <cstring>

#include <chrono>
using namespace std::chrono;

void basic_tests(){
  BinaryHeap<int> bh;

  try{
    bh.top();
    assert("No exception" == nullptr);
  }
  catch(const std::out_of_range &){}
  catch(...){
    assert("Wrong exception" == nullptr);
  }

  try{
    bh.pop();
    assert("No exception" == nullptr);
  }
  catch(const std::out_of_range &){}
  catch(...){
    assert("Wrong exception" == nullptr);
  }

  bh.push(10);
  assert(bh.top() == 10);
  bh.push(1);
  bh.push(10);
  bh.push(7);
  bh.push(2);
  bh.push(100);
  bh.push(-10);
  bh.push(0);
  assert(bh.top() == -10);
  assert(bh.pop() == -10);

  assert(bh.top() == 0);
  assert(bh.pop() == 0);

  assert(bh.top() == 1);
  assert(bh.pop() == 1);

  assert(bh.top() == 2);
  assert(bh.pop() == 2);

  assert(bh.top() == 7);
  assert(bh.pop() == 7);

  assert(bh.top() == 10);
  assert(bh.pop() == 10);

  assert(bh.top() == 10);
  assert(bh.pop() == 10);

  assert(bh.top() == 100);
  assert(bh.pop() == 100);

  try{
    bh.top();
    assert("No exception" == nullptr);
  }
  catch(const std::out_of_range &){}
  catch(...){
    assert("Wrong exception" == nullptr);
  }

  try{
    bh.pop();
    assert("No exception" == nullptr);
  }
  catch(const std::out_of_range &){}
  catch(...){
    assert("Wrong exception" == nullptr);
  }

  std::cout << "Passed first basic tests" << std::endl;
}

void basic_tests_2(){
  BinaryHeap<double, std::greater<double>> bh;

  bh.push(1);
  bh.push(2);
  bh.push(2);
  bh.push(1);
  bh.push(10);
  bh.push(10);

  assert(bh.top() == 10);
  assert(bh.pop() == 10);

  assert(bh.top() == 10);
  assert(bh.pop() == 10);

  assert(bh.top() == 2);
  assert(bh.pop() == 2);

  assert(bh.top() == 2);
  assert(bh.pop() == 2);

  assert(bh.top() == 1);
  assert(bh.pop() == 1);

  assert(bh.top() == 1);
  assert(bh.pop() == 1);

  try{
    bh.top();
    assert("No exception" == nullptr);
  }
  catch(const std::out_of_range &){}
  catch(...){
    assert("Wrong exception" == nullptr);
  }

  try{
    bh.pop();
    assert("No exception" == nullptr);
  }
  catch(const std::out_of_range &){}
  catch(...){
    assert("Wrong exception" == nullptr);
  }

  std::cout << "Passed second basic tests" << std::endl;
}

void random_data_tests(int MIN, int MAX, size_t NUM) {
  BinaryHeap<int> bh{};
  std::priority_queue<int, std::vector<int>, std::greater<int>> que;
  auto start = high_resolution_clock::now();

  char c;
  for(size_t i = 0; i < NUM; ++i){
    c = rand() % 3;

    switch(c){
      case 0: {
        int x = rand() % abs(MIN - MAX) - MIN;
        bh.push(x);
        que.push(x);
      }
        break;
      case 1:
        try{
          assert(bh.top() == que.top());
        }
        catch(const std::out_of_range &){
          assert(bh.size() == 0 && que.size() == 0);
        }
        break;
      case 2:
        try{
          assert(bh.pop() == que.top());
          que.pop();
        }
        catch(const std::out_of_range &){
          assert(bh.size() == 0 && que.size() == 0);
        }
        break;
    }
  }
  auto stop = high_resolution_clock::now();
  auto duration = duration_cast<microseconds>(stop - start);
  std::cout << "Passed random tests for " << NUM << " values.\n\tTook " << duration.count() << " miliseconds." << std::endl;
}

void template_tests(){
  struct FCmp{
    FCmp(bool arev = false) :
      reverse(arev) {}
    bool operator()(const std::string & l, const std::string & r) {
      return reverse ^ strcasecmp(l.c_str(), r.c_str());
    }

    bool reverse = false;
  };

  BinaryHeap<std::string, FCmp, std::deque<std::string>> bh0{FCmp(true)};

  bh0.push("bbb");
  bh0.push("aaa");
  bh0.push("AAA");

  assert(bh0.pop() == "aaa");
  assert(bh0.pop() == "AAA");
  assert(bh0.pop() == "bbb");

  std::cout << "Passed template tests" << std::endl;
}

int main() {
  basic_tests();
  basic_tests_2();
  template_tests();
  random_data_tests(100, 1000, 1000);
  random_data_tests(-1000, 1000, 50000);
  random_data_tests(-10000, 100000, 9999999);
  random_data_tests(-100, 100, 99999);
}
