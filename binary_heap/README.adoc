== Binary heap

Implement templated binary heap structure. Where top evalutes true as left hand side with every element of heap using custon comparator.

.Methods and requirments to implement
* Templated datatype, comparator and container. Last two defaulted to std::less and std::vector
* *push* method in O(log n) amortized
* *top* method in O(1), throws std::out_of_range on empty heap
* *pop* method in O(log n) amortized, throws std::out_of_range on empty heap
* *size* method in O(1)

This repository contains file that shows needed interface.
